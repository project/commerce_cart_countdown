/*
 * @file
 * JavaScript for Countdown Timer.
 *
 * Checking the countdown timer
 */
jQuery(document)
  .ready(
    function () {
      'use strict';
      var t = jQuery('#cartTimeoutMessage')
        .html();
      var upgradeTime = t;
      var seconds = upgradeTime;
      if (jQuery('#hidCountdown')
        .val() !== 'cart empty') {
        var countdownTimer = setInterval(
          function () {
            var days = Math.floor(seconds / 24 / 60 / 60);
            var hoursLeft = Math.floor((seconds) - (days * 86400));
            var hours = Math.floor(hoursLeft / 3600);
            var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
            var minutes = Math.floor(minutesLeft / 60);
            var remainingSeconds = seconds % 60;
            if (remainingSeconds < 10) {
              remainingSeconds = '0' + remainingSeconds;
            }
            jQuery('#countdown')
              .html(minutes + ':' + remainingSeconds + ' <span>left</span>');
            if (seconds === 0) {
              jQuery('#cartTimeoutMessageContent')
                .show();
              clearInterval(countdownTimer);
              showHint();
            }
            else {
              seconds--;
            }
          }, 1000
        );
      }
    }
  );
// Ajax callback function after complete the countdown time.
function showHint() {
  'use strict';
  var xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (xhttp.readyState === 4 && xhttp.status === 200) {
      document.getElementById('countdown')
        .innerHTML = xhttp.responseText;
    }
  };
  xhttp.open('GET', 'clearcart/ajax', true);
  xhttp.send();
}
