<?php

/**
 * @file
 * Provides a time-based cart expiration feature.
 */

/**
 * Implements hook_help().
 */
function commerce_cart_countdown_timer_help($path, $arg) {
  switch ($path) {
    case 'admin/help#commerce_cart_countdown_timer':

      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}

/**
 * Impelments hook_menu()
 */
function commerce_cart_countdown_timer_menu() {
  $items['clearcart/ajax'] = array(
    'page callback' => 'commerce_cart_countdown_timer_clearcartajax',
    // Anonymous user also can access this.
    'access callback' => TRUE,
  );
  $items['admin/commerce/config/commerce_cart_countdown_timer'] = array(
    'title' => 'Cart count down timer configuration',
    'description' => 'Cart time settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_cart_countdown_timer_cart_time_settings'),
    'access arguments'  => array('access cart time settings form'),
    'weight' => 10,
  );
  return $items;
}

/**
 * Form Call back function for Set Time.
 */
function commerce_cart_countdown_timer_cart_time_settings($form, &$form_state) {
  $form = array();
  $form['commerce_cart_countdown_timer_expiry_time'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('commerce_cart_countdown_timer_expiry_time', '600'),
    '#title' => t('Cart expiry time'),
    '#options' => array(
      '60' => t('1 minute'),
      '120' => t('2 minutes'),
      '300' => t('5 minutes'),
      '600' => t('10 minutes'),
      '900' => t('15 minutes'),
      '1200' => t('20 minutes'),
    ),
    '#required' => TRUE,
  );
  $default_cart_time_out_message = variable_get('commerce_cart_countdown_timer_alert_message', '<p>ALERT</p><p>Your cart has timed out.</p>');
  $form['commerce_cart_countdown_timer_alert_message'] = array(
    '#type' => 'text_format',
    '#default_value' => isset($default_cart_time_out_message['value']) ? $default_cart_time_out_message['value'] : $default_cart_time_out_message,
    '#title' => t('Cart time out alert message'),
    '#rows' => 10,
    '#format' => isset($default_cart_time_out_message['format']) ? $default_cart_time_out_message['format'] : 'full_html',
    '#required' => TRUE,
  );
  $form['commerce_cart_countdown_timer_start_over_button_text'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_cart_countdown_timer_start_over_button_text', 'Start Over'),
    '#title' => t('Start over button text'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
  // Sets up a form to save information automatically.
}

/**
 * Ajax callback function .
 */
function commerce_cart_countdown_timer_clearcartajax() {
  global $user;
  // Load the current shopping cart order.
  $order = commerce_cart_order_load($user->uid);

  // Delete the order, if found.
  if (!empty($order)) {
    commerce_order_delete($order->order_id);
    db_delete('commerce_cart_countdown_timer')
      ->condition('cart_order_id', $order->order_id)
      ->execute();
    $message = t('Cart empty');
  }
  else {
    $message = t('The cart was already empty.');
  }
  print $message;
}

/**
 * Implements hook_form_alter().
 */
function commerce_cart_countdown_timer_form_alter(&$form, &$form_state, $form_id) {
  // Added cart created time while loading the cart.
  if ($form_id == 'views_form_commerce_cart_form_default') {
    global $user;
    $order = commerce_cart_order_load($user->uid);
    // Checking order id already added or not in table.
    if (isset($order->order_id)) {
      $subquery = db_select('commerce_cart_countdown_timer', 'clt')
        ->fields('clt', array('cart_order_id'))
        ->condition('clt.cart_order_id', $order->order_id)
        ->execute();
      if ($subquery->rowCount() == 0) {
        db_insert('commerce_cart_countdown_timer')
          ->fields(
            array(
              'cart_order_id' => $order->order_id,
              'created' => time(date('Y-m-d H:i:s')
              ),
            )
        )->execute();
      }
    }
  }
}

/**
 * Implements hook_block_info().
 *
 * Added the Cart Countdown timer function.
 */
function commerce_cart_countdown_timer_block_info() {
  $blocks['cart_countdown_timer'] = array(
    'info' => t('Cart countdown timer display'),
    'status' => TRUE,
  // Default enabled status.
    'region' => 'content',
  // Default region.
    'weight' => '-100',
  // Weight of block (position).
    'cache' => DRUPAL_NO_CACHE,
  // Default setting.
  );
  return $blocks;
}

/**
 * Hook_block_view() loading the block content.
 */
function commerce_cart_countdown_timer_block_view($delta = '') {
  switch ($delta) {
    case 'cart_countdown_timer':
      $block['subject'] = '';
      $block_countent = commerce_cart_countdown_timer_block_content();
      // Function call return the out of block.
      if (empty($block_countent)) {
        $block['content'] = t('No Time.');
      }
      else {
        $block['content'] = $block_countent;
      }
      return $block;
  }
}

/**
 * Load countdown timer as block content.
 */
function commerce_cart_countdown_timer_block_content() {
  global $base_url;
  // Get specified module path.
  $path = drupal_get_path('module', 'commerce_cart_countdown_timer');
  // Added the js.
  drupal_add_js($path . '/js/cart_countdown_timer.js');
  global $user;
  // Loading the order based on user id.
  $order = commerce_cart_order_load($user->uid);
  // Check if order exist or not.
  if (isset($order->order_id)) {
    $query = db_select('commerce_cart_countdown_timer', 'clt')
      ->fields('clt', array('cart_order_id', 'created'))
      ->condition('clt.cart_order_id', $order->order_id, '=');
    $result = $query->execute();
    // $defultStartTime = 9999;.
    $defultStartTime = 0;
    // Set Sart (default) Time for countdown timer.
    while ($record = $result->fetchAssoc()) {
      $defultStartTime = abs(strtotime(date('Y-m-d H:i:s')) - $record["created"]);
    }
    $minutes = round($defultStartTime);
    // Calculated countdown timer.
    $maxTime = variable_get('commerce_cart_countdown_timer_expiry_time', '600');
    // Get/Set the Max cart time out.
    if ($minutes > $maxTime) {
      db_delete('commerce_cart_countdown_timer')
        ->condition('cart_order_id', $order->order_id)
        ->execute();
      // Delete all time from  cart.
      commerce_cart_order_empty($order);
    }
    else {
      $minutes = $maxTime - $minutes;
      // Timer difference from cart added time.
    }
    // Countdown timer cart output HTML text.
    $cart_time_out_message = variable_get('commerce_cart_countdown_timer_alert_message', '<p>ALERT</p><p> Your cart has timed out.</p>');
    $cart_time_out_message_text = isset($cart_time_out_message['value']) ? $cart_time_out_message['value'] : $cart_time_out_message;
    $cart_time_out_message_format = isset($cart_time_out_message['format']) ? $cart_time_out_message['format'] : 'full_html';
    $cart_time_out_message_markup = check_markup($cart_time_out_message_text, $cart_time_out_message_format);
    $outputText = '<div id="cartTimeoutMessage" style="display:none;">' . $minutes . '</div>';
    $outputText .= '<div id="cartTimeoutMessageContent" style="display:none;"><div id="cartMessage" class="expire-cart-pop-msg"><div class="txt">' . $cart_time_out_message_markup . '</div><a href="' . $base_url . '">' . variable_get('commerce_cart_countdown_timer_start_over_button_text', 'Start Over') . '</a></div></div>';

    // Check if cart is empty or not.
    $cart_count = commerce_cart_countdown_timer_get_cart_count();
    if ($cart_count == 0) {
      $outputText .= '<input type="hidden" id="hidCountdown" name="hidCountdown" value="cart empty"/><div id="countdown" style="display:none!important;" class="countdown">cart empty</div>';
    }
    else {
      $outputText .= '<input type="hidden" id="hidCountdown" name="hidCountdown" value=""/><div><a href="' . $base_url . '/cart"><div id="countdown" class="countdown">&nbsp;</div></a></div>';
    }
  }
  else {
    $outputText = '<input type="hidden" id="hidCountdown" name="hidCountdown" value="cart empty"/><div id="countdown" style="display:none!important;" class="countdown">cart empty</div>';
  }
  return $outputText;
  // Return output to countdown block.
}

/**
 * Get the cart item count .
 */
function commerce_cart_countdown_timer_get_cart_count() {
  global $user;
  $quantity = 0;
  // Load the order based on user.
  $order = commerce_cart_order_load($user->uid);
  if ($order) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $wrapper->commerce_line_items;
    // Get the line item quantity from cart based on user.
    $quantity = commerce_line_items_quantity($line_items, commerce_product_line_item_types());
  }
  return $quantity;
}

/**
 * After complete the checkout delete the cart life table order id .
 */
function commerce_cart_countdown_timer_commerce_checkout_complete($order) {
  db_delete('commerce_cart_countdown_timer')
    ->condition('cart_order_id', $order->order_id)
    ->execute();
}
