CONTENTS OF THIS FILE
---------------------

 	* Introduction
 	* Requirements
 	* Installation
 	* Configuration
 	* Maintainers


INTRODUCTION
------------
	This module is used for cart timeout functionality.
	Set the time limit after adding the cart item(s), the user can only buy the
	product/purchase the event within a time limit.

	Display the cart countdown timer on the page. When we
	add the products line item into the cart, countdown timer will start
	automatically. Once countdown timer expires, the cart items will be cleared
	automatically.


REQUIREMENTS
------------
 	* Drupal Commerce (http://drupal.org/project/commerce): 7.x-1.0 or newer


INSTALLATION
------------
	* Install as you would normally install a contributed drupal module. See:
   	https://drupal.org/documentation/install/modules-themes/modules-7
  	for further information.

 	* We can change the default(content) block region configuration, under
    Administration » Structure » Blocks


CONFIGURATION
-------------
	* Go to the following URL to set the cart timeout settings.
    admin/commerce/config/commerce_cart_countdown_timer
    Following settings are there:
   	* Cart expiry time
  	* Cart time out alert message 
   	* Start over button text


MAINTAINERS
-----------

 	* Siva Prasad C (Sivaprasad C) - https://www.drupal.org/u/sivaprasad-c

Supporting organization:

 	* Drupal Partners - https://www.drupal.org/drupal-partners-0

	Our Services include Drupal Development, Drupal eCommerce Development, Drupal
	Migration, Drupal Maintenance, Drupal Multisites, Drupal Intranet.
